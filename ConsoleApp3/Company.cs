﻿using System;

namespace ConsoleApp3
{
    public class Company : IManager, IFinanse
    {
        public void GetCountOfManagers()
        {
            Console.WriteLine($"Here we want to get count of managers");
        }

        public void GetCountOfFinansists()
        {
            Console.WriteLine($"Here we want to get count of finansists");
        }
    }

    public interface IManager
    {
        void GetCountOfManagers();
    }

    public interface IFinanse
    {
        void GetCountOfFinansists();
    }
}
