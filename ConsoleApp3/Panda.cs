﻿namespace ConsoleApp3
{
    public class Panda
    {
        public static int Generation = 0;

        public Panda()
        {
            Generation += 1;
        }

        public Panda(int generationCount)
        {
            Generation += generationCount;
        }
    }
}
