﻿namespace ConsoleApp3
{
    public class GenericConvertationForClasses<T> where T : class
    {
        private T _data;

        public T Data
        {
            get { return this._data; }
            set { this._data = value; }
        }
    }
}
