﻿using ConsoleApp3.Animals;
using System;
using System.Collections.Generic;

namespace ConsoleApp3
{
    public class Program
    {
        static void Main(string[] args)
        {
            //List<int> list = new List<int>();

            GenericConvertationClass<string, int> firstGeneric = new GenericConvertationClass<string, int>
            {
                ClassData = "Some test data",
                StructData = 5
            };

            GenericConvertationClass<Dog, double> secondGeneric = new GenericConvertationClass<Dog, double>
            {
                ClassData = new Dog(),
                StructData = 5.4
            };


            GenericConvertationForClasses<Cat> catGeneric = new GenericConvertationForClasses<Cat>()
            {
                Data = new Cat()
            };

            catGeneric.Data.AnimalVoice();


            //Console.WriteLine(Panda.Generation);

            //Panda p1 = new Panda();
            //Console.WriteLine(Panda.Generation);
            //Panda.Generation += 2;
            //Console.WriteLine(Panda.Generation);

            //Panda p2 = new Panda(3);
            //Console.WriteLine(Panda.Generation);

            Console.ReadKey();
        }


        private static void Inheritance()
        {
            City city = new City();
            city.ShowStateMessage();

            Console.WriteLine();

            city.ShowCityMessage();
        }

        private static void InterfaceInheritance()
        {
            Company company = new Company();
            company.GetCountOfFinansists();
            company.GetCountOfManagers();

            Console.WriteLine();

            IFinanse finanse = new Company();
            finanse.GetCountOfFinansists();

            Console.WriteLine();

            IManager manager = new Company();
            manager.GetCountOfManagers();
        }

        private static void MethodOverloading()
        {
            Overloading over = new Overloading();

            Console.WriteLine(over.Add(1, 2));
            Console.WriteLine();

            Console.WriteLine(over.Add(1, 2, 3));
            Console.WriteLine();

            Console.WriteLine(over.Add(1.1, 2.2, 3.3, 4.5));
        }
    }
}
