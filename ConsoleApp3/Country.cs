﻿using System;

namespace ConsoleApp3
{
    public class Country
    {
        public string CountryName { get; set; } = "USA";
        private string CountryCode { get; set; } = "US";
    }

    public class State: Country
    {
        public string StateName { get; set; } = "New York";
        private string ShortStateName { get; set; } = "NY";

        public void ShowStateMessage()
        {
            Console.WriteLine($"State {StateName} is in country {CountryName}");
        }
    }

    public class City : State
    {
        public string CityName { get; set; } = "New York City";
        public double CoordinateX = 5.42;
        public double CoordinateY = 45.76;

        public void ShowCityMessage()
        {
            Console.WriteLine($"City {CityName} is in state {StateName}");
            Console.WriteLine($"State {StateName} is in country {CountryName}");
        }
    }
}
