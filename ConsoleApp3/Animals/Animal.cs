﻿using ConsoleApp3.Enums;

namespace ConsoleApp3.Animals
{
    public abstract class Animal
    {
        private string _name;

        public abstract int Age { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _name = "default test name";
                }
                else
                {
                    _name = value;
                }
            }
        }
        public Size Size { get; set; }
        public AnimalAction Action { get; set; }

        public abstract void AnimalVoice();
    }
}
