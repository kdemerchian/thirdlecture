﻿using ConsoleApp3.Enums;
using System;

namespace ConsoleApp3.Animals
{
    public class Mammal : Animal
    {
        public Mammal()
        {
            FoodPreferences = FoodPreferences.Predator;
        }

        public Mammal(FoodPreferences foodPreferences)
        {
            FoodPreferences = foodPreferences;
        }

        public FoodPreferences FoodPreferences { get; private set; }

        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public override void AnimalVoice()
        {
            if (FoodPreferences.Equals(FoodPreferences.Predator))
            {
                Console.WriteLine("Grrrrrhhh");
            }
            else
            {
                Console.WriteLine("Muuuuuuuu");
            }
        }
    }
}
