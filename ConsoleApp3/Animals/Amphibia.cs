﻿using ConsoleApp3.Enums;
using System;

namespace ConsoleApp3.Animals
{
    public class Amphibia : Animal, IForecast, IForecast2
    {
        public AmphibiaType Type { get; set; }

        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public override void AnimalVoice()
        {
            Console.WriteLine("No comments");
        }

        int IForecast.GetForecast(int value)
        {
            return value + 10;
        }

        int IForecast2.GetForecast(int value)
        {
            return 2 * value + 10;
        }
    }
}
