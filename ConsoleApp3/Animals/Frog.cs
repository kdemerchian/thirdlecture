﻿using System;

namespace ConsoleApp3.Animals
{
    public class Frog : Amphibia
    {
        public override void AnimalVoice()
        {
            Console.WriteLine("Double kva");
        }


        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value * 15; }
        }

        public void DoSleepAllYear()
        {
            Console.WriteLine("Sleep all year");
        }
    }
}
