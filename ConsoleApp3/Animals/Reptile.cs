﻿using System;
using ConsoleApp3.Enums;

namespace ConsoleApp3.Animals
{
    public class Reptile : Animal
    {
        public ReptileType Type { get; set; }

        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value * 10; }
        }

        public override void AnimalVoice()
        {
            Console.WriteLine("Ssssssss");
        }
    }
}
