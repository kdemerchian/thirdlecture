﻿using System;

namespace ConsoleApp3.Animals
{
    public class Dog : Mammal
    {
        public override void AnimalVoice()
        {
            Console.WriteLine("Gav gav");
        }

        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value * 3; }
        }

        public void EatMeat()
        {
            Console.WriteLine("Eat meat");
        }

    }
}
