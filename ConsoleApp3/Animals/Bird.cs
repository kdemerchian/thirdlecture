﻿using ConsoleApp3.Enums;
using System;

namespace ConsoleApp3.Animals
{
    public class Bird : Animal
    {
        public Bird(FoodPreferences foodPreferences)
        {
            FoodPreferences = foodPreferences;
        }

        public double BeakLength { get; set; }
        public FoodPreferences FoodPreferences { get; private set; }

        private int _age;
        public override int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public override void AnimalVoice()
        {
            if (FoodPreferences.Equals(FoodPreferences.Predator))
            {
                Console.WriteLine("Kar kar");
            }
            else
            {
                Console.WriteLine("Ko ko ko");
            }
        }

        public void EatCorn()
        {
            Console.WriteLine("Eat corn");
        }
    }
}
