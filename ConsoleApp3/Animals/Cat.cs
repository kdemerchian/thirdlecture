﻿using System;

namespace ConsoleApp3.Animals
{
    public class Cat : Mammal
    {
        public override void AnimalVoice()
        {
            Console.WriteLine("Myau myau");
        }

        public void EatRats()
        {
            Console.WriteLine("Eat rat");
        }

        public void EatMouse()
        {
            Console.WriteLine("Eat mouse");
        }

        public void EatFish()
        {
            Console.WriteLine("Eat fish");
        }
    }
}
