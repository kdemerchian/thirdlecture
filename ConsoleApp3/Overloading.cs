﻿namespace ConsoleApp3
{
    public class Overloading
    {
        public int Add(int a, int b) //Method 1   
        {
            return a + b;
        }

        public int Add(int a, int b, int c) //Method 2  
        {
            return a + b + c;
        }

        public double Add(double a, double b, double c, double d) //Method 3  
        {
            return a + b + c + d;
        }
    }
}
