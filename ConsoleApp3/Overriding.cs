﻿namespace ConsoleApp3
{
    public class Overriding
    {
        public virtual int Balance()
        {
            return 10;
        }
    }
    public class Amount : Overriding
    {

        public override int Balance()
        {
            return 500;
        }
    }
}
