﻿namespace ConsoleApp3
{
    public class GenericConvertationClass<T, U>
        where T : class 
        where U : struct
    {
        private T _classData;
        private U _structData;

        public T ClassData
        {
            get { return this._classData; }
            set { this._classData = value; }
        }

        public U StructData
        {
            get { return this._structData; }
            set { this._structData = value; }
        }


    }
}
