﻿namespace ConsoleApp3.Enums
{
    public enum AnimalAction
    {
        Crawl,
        Jump,
        Go,
        Swim,
        Fly
    }
}
