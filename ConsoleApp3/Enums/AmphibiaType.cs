﻿namespace ConsoleApp3.Enums
{
    public enum AmphibiaType
    {
        Triton,
        Salamander,
        Frog,
        Worm
    }
}
